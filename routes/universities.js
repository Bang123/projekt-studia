var express = require("express");
var router  = express.Router();
var University = require("../models/university");
var Company     = require("../models/company");
var middleware = require("../middleware");
var request = require("request");

//INDEX - show all universities
router.get("/", function(req, res){
    console.log("SZUKAM WSZYSTKICH UNI");
    University.find({}).populate("companies.item").exec(function(err, allUniversities) {
        if(err) {
            console.log("error");
        }
        else {
            res.send(allUniversities);
        }
    });
});

//CREATE - add new university to DB
router.post("/", middleware.isLoggedIn, function(req, res){
    // get data from form and add to campgrounds array
    console.log(req.body);
    var name = req.body.name;
    var image = req.body.image;
    var description = req.body.description;
    var newUniversity = {name: name, img: image, description: description};
    // Create a new university and save to DB
    University.create(newUniversity, function(err, newlyCreated){
        if(err){
            console.log(err);
        } else {
            //redirect back to university page
            console.log("Dodano nowa uczelnie");
            res.redirect("/#/uczelnie/new");
        }
    });
});

// SHOW - shows more info about one university
router.get("/:id", function(req, res){
    console.log("SZUKAM KONKRETNEGO UNI");
    University.findById(req.params.id).populate("companies.item").exec(function(err, foundUniversity){
        if(err){
            console.log(err);
        } else {
            res.send({foundUniversity: foundUniversity, currentUser: res.locals.currentUser});
        }
    });
});

//EDIT - show edit form to one university
router.get("/:id/edit", middleware.isLoggedIn, function(req, res){
    console.log("IN EDIT!");
    //find the campground with provided ID
    University.findById(req.params.id, function(err, foundUniversity){
        if(err){
            console.log(err);
        } else {
            //render show template with that campground
            res.send(foundUniversity);
        }
    });
});

//EDIT - put edited info about one university
router.put("/:id", middleware.isLoggedIn, function(req, res){
    var newData = {name: req.body.name, img: req.body.image, description: req.body.description};
    console.log(newData);
    console.log("Tutaj jest id:" + req.params.id);
    University.findByIdAndUpdate(req.params.id, {$set: newData}, function(err, university){
        if(err){
            console.log(err);
            res.redirect("back");
        } else {
            res.redirect("/#/uczelnie/" + university.id);
        }
    });
});

//DELETE - delete university
router.delete("/:universityId", middleware.isLoggedIn, function(req, res){
    University.findById(req.params.universityId).populate("companies.item").exec(function(err, university) {
        if(err) {
            console.log(err);
            res.redirect("/uczelnie");
        } else {
            var numOfCompanies = university.companies.length;
            console.log(numOfCompanies + " na zewntarz ");
            if(numOfCompanies === 0) {
                deleteUni(res, university);
            } else {
                university.companies.forEach(function(company) {
                   Company.findById(company.item.id).populate("universities.item").exec(function(err, foundCompany) {
                       if(err) {
                           console.log("nie znalazlem uniwersytetu w firmie");
                       } else {
                           for(var i = 0; i < foundCompany.universities.length; i++) {
                               console.log(i + " iteracja");
                               console.log(foundCompany);
                               console.log(foundCompany.universities[i].item.id === req.params.universityId);
                                if(foundCompany.universities[i].item.id === req.params.universityId) {
                                    foundCompany.universities.splice(i, 1);
                                    break;
                                }
                            }
                            foundCompany.save(function() {
                                numOfCompanies = numOfCompanies - 1;
                                console.log(numOfCompanies);
                                if(numOfCompanies === 0) {
                                    deleteUni(res, university);
                                }
                            });
                       }
                   });
                });
            }
        }
    });
});

module.exports = router;

var deleteUni = function(res, uni) {
    University.findByIdAndRemove(uni.id, function(err) {
        if(err) {
            console.log(err);
        } else {
            console.log("usunalem uni");
            res.redirect("/#/uczelnie");
        }
    });
};
